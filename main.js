const { app, BrowserWindow, ipcMain, shell } = require('electron')

const os = require('os')
const path = require('path')
const fs = require('fs')
const YAML = require('yaml')

process.starkeeper = {
  userHome: os.homedir(),
  pods: {}
}

const loadPods = () => {
  try {
    const pods = fs.readdirSync('pods')

    for (const podDirectory of pods) {
      if (podDirectory === 'main') continue
      const podYaml = path.join('pods', podDirectory, 'pod.yaml')
      if (fs.existsSync(podYaml)) {
        const podSpawn = {
          name: 'unSpawned',
          main: path.join(podDirectory, 'main.js'),
          preload: path.join(podDirectory, 'preload.js'),
          renderer: path.join(podDirectory, 'render.js')
        }
        const yamlSrc = fs.readFileSync(podYaml, 'utf8')
        const config = YAML.parse(yamlSrc)
        process.starkeeper.pods[podDirectory] = { ...podSpawn, ...config }
        if (fs.existsSync(podSpawn.main)) {
          require(podSpawn.main)
        }
      }
    }
  } catch (e) {
    console.error(e)
  }
}

const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: 'preload.js'
    }
  })

  loadPods()
  win.webContents.openDevTools()
  win.loadFile('index.html')
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
})

const podEvent = (event, data) => {}

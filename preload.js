const { contextBridge, ipcRenderer } = require('electron')
const { debug } = require('yaml/util')

contextBridge.exposeInMainWorld('starkeeperAPI', {
  podEvent: () => ipcRenderer.invoke('handlePodEvent')
})

makeUIButton = props => {
  return `<li><p>
        <a class="button ${props.classes}" href="${props.url}">
          <span><img class="launcher-icon" src="${props.icon}"/> ${props.name}</span>
        </a>
    </p></li>`
}

console.log('main.preload')
const items = ['<nav class="horizontal"><ul>']
for (const pod of pods) {
  items.push(makeUIButton(pod))
}
items.push('  </ul></nav>')
console.log(items)
const html = items.join('/n')
document.getElementById('podAttachmentSite').innerHTML = html

const { app, BrowserWindow, ipcMain, shell } = require('electron')
const os = require('os')
const path = require('path')
const fs = require('fs')

const homeDirectory = os.homedir()
const desktopPath = path.join(homeDirectory, 'Desktop')
const defaultConfig = {
  extractionPath: desktopPath,
  outputPath: path.join(desktopPath, 'StarControlExtractedIcons')
}
ipcMain.handle('extractIcons', extractIcons)
ipcMain.handle('getDefaultValues', () => {
  return defaultConfig
})

const getIconFromShortcut = (iconPath, outputDirectory) => {
  const lines = fs.readFileSync(iconPath, 'utf8').split('\r\n')
  for (const line of lines) {
    if (!line.startsWith('[') && line.indexOf('=') !== 0) {
      const [key, value] = line.split('=')
      if (key == 'IconFile') {
        if (fs.existsSync(value)) {
          const icoData = fs.readFileSync(value)
          const outFileName = path.basename(iconPath, '.url') + '.ico'
          const outFilePath = path.join(outputDirectory, outFileName)
          fs.writeFileSync(outFilePath, icoData, 'binary')
          return { name: outFileName, data: icoData }
        }
      }
    }
  }
}

const extractIcons = (event, config) => {
  try {
    const response = []
    const iconLoaded = data => {
      const inFileExt = path.extname(data.Path)
      const inFileName = path.basename(data.Path, inFileExt)
      const outFileName = `${inFileName}.png`
      const outFilePath = path.join(config.outputPath, outFileName)
      fs.writeFileSync(outFilePath, data.Base64ImageData, 'base64')
      response.push({ name: outFileName, image: data.Base64ImageData })
    }

    iconExtractor.emitter.on('icon', iconLoaded)
    const results = []
    const files = fs.readdirSync(config.extractionPath)
    if (!fs.existsSync(config.outputPath)) {
      fs.mkdirSync(config.outputPath)
    }
    for (const file of files) {
      const fileExt = path.extname(file)
      const iconPath = path.join(config.extractionPath, file)
      if (fileExt === '.url') {
        const result = getIconFromShortcut(iconPath, config.outputPath)
        response.push(result)
      } else if (fileExt === '.exe') {
        // iconExtractor.getIcon('starkeeper', iconPath)
      } else if (fileExt === '.lnk') {
        const parsed = shell.readShortcutLink(iconPath)
        // iconExtractor.getIcon('starkeeper', parsed.target)
      }
    }
    return response
  } catch (error) {
    console.error(error)
  }
}

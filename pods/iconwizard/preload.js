const { contextBridge, ipcRenderer } = require('electron')

module.exports = preload = () => {
  contextBridge.exposeInMainWorld('starkeeperAPI', {
    getDefaultValues: () => ipcRenderer.invoke('getDefaultValues'),
    extractIcons: config => ipcRenderer.invoke('extractIcons', config)
  })

  window.addEventListener('DOMContentLoaded', () => {
    const replaceText = (selector, text) => {
      const element = document.getElementById(selector)
      if (element) element.innerText = text
    }

    for (const dependency of ['chrome', 'node', 'electron']) {
      replaceText(`${dependency}-version`, process.versions[dependency])
    }

    console.log('preload', process.starkeeperAPI.config)

    const inputText = document.getElementById('extractionPath')
    if (inputText)
      inputText.setAttribute(
        'value',
        process.starkeeperAPI.config.extractionPath
      )

    const outputText = document.getElementById('outputPath')
    if (outputText)
      outputText.setAttribute('value', process.starkeeperAPI.config.outputPath)
  })
}

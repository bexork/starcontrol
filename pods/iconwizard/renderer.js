const getDefaultValues = async () => {
  const starkeeper = await window.starkeeperAPI.getDefaultValues()
  const inputText = document.getElementById('extractionPath')
  if (inputText) {
    inputText.setAttribute('value', starkeeper.extractionPath)
  }

  const outputText = document.getElementById('outputPath')
  if (outputText) {
    outputText.setAttribute('value', starkeeper.outputPath)
  }
}

const executeButton = document.getElementById('extractIcons')
executeButton.onclick = async () => {
  const inputText = document.getElementById('extractionPath')
  const outputText = document.getElementById('outputPath')
  const result = await window.starkeeperAPI.extractIcons({
    extractionPath: inputText.value,
    outputPath: outputText.value
  })
  const output = document.getElementById('output')
  //output.innerHTML = `<pre>${result.join('\r')}</pre>`
  const html = []
  for (const image of result) {
    html.push(`<div>  <img src="data:image/png;base64, ${image.data}"/> </div>`)
  }
  output.innerHTML = html.join()
}

getDefaultValues()
